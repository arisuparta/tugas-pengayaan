<?php  

class Dosen_model 
{
    private $table = 'dosen';
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    // function menampilkan semua data dosen
    public function getAllDosen()
    {
        $this->db->query('SELECT * FROM ' . $this->table);
        return $this->db->resultSet();
    }

    // function menampilkan data dosen berdasarkan id
    public function getDosenByID($id)
    {
        $this->db->query('SELECT * FROM ' . $this->table . ' WHERE id_dosen=:id_dosen ');
        $this->db->bind('id_dosen', $id);
        return $this->db->single();
    }

    // function menambah data dosen
    public function tambahDataDosen($data)
    {
        // mengupload gambar dan mengambil nama gambar
        $gambar = $this->upload();

        $query = "INSERT INTO dosen
                    VALUES
                    (null,'$gambar', :nip, :nama, :prodi, :fakultas)";

        $this->db->query($query);
        $this->db->bind('nama', $data['nama']);
        $this->db->bind('nip', $data['nip']);
        $this->db->bind('prodi', $data['prodi']);
        $this->db->bind('fakultas', $data['fakultas']);

        $this->db->execute();

        return $this->db->rowCount();
    }


    // function menambah data dosen
    public function editDataDosen($data)
    {
        // mengupload gambar dan mengambil nama gambar
        $gambar = $this->upload();

        $query = "UPDATE dosen SET
                    foto_dosen = '$gambar',
                    nip_dosen = :nip,
                    nama_dosen = :nama,
                    prodi = :prodi,
                    fakultas = :fakultas
                WHERE id_dosen = :id_dosen";

        $this->db->query($query);
        $this->db->bind('nama', $data['nama']);
        $this->db->bind('nip', $data['nip']);
        $this->db->bind('prodi', $data['prodi']);
        $this->db->bind('fakultas', $data['fakultas']);
        $this->db->bind('id_dosen', $data['id_dosen']);

        $this->db->execute();

        return $this->db->rowCount();
    }
    

    // function upload file (foto_dosen)
    function upload()
    {
        $namaFile = trim($_FILES['foto']['name']);
        $tmpName = $_FILES['foto']['tmp_name'];

        // cek file
        $ekstensiGambarValid = ['jpg', 'jpeg', 'png'];
        $ekstensiGambar = explode('.', $namaFile);
        $ekstensiGambar = strtolower(end($ekstensiGambar));

        if (!in_array($ekstensiGambar, $ekstensiGambarValid)) {
            echo "<script>
                alert('yang anda upload bukan gambar!');
            </script> ";
            return false;
        }

        // upload
        move_uploaded_file($tmpName, '../public/img/foto_dosen/' . $namaFile);
        return $namaFile;
    }

    // function hapus data dosen
    public function hapusDataDosen($id)
    {
        $query = "DELETE FROM dosen WHERE id_dosen =:id_dosen";
        $this->db->query($query);
        $this->db->bind('id_dosen', $id);

        $this->db->execute();

        return $this->db->rowCount();
    }
}
?>