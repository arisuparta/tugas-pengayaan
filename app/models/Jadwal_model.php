<?php 

class Jadwal_model
{
    private $table = 'jadwal_kelas';
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    // function menampilkan semua data Jadwal
    public function getAllJadwal()
    {
        $this->db->query('SELECT * FROM jadwal_kelas JOIN dosen ON jadwal_kelas.id_dosen = dosen.id_dosen  JOIN kelas ON jadwal_kelas.id_kelas = kelas.id_kelas');
        return $this->db->resultSet();
    }

    // function menampilkan data jadwal berdasarkan id 
    public function getJadwalByID($id)
    {
        $this->db->query('SELECT * FROM ' . $this->table . ' WHERE id_jadwal=:id_jadwal ');
        $this->db->bind('id_jadwal', $id);
        return $this->db->single();
    }

    // function menambah data jadwal kelas
    public function tambahDataJadwal($data)
    {

        $query = "INSERT INTO jadwal_kelas
                    VALUES
                    (null,:id_dosen, :id_kelas, :jadwal, :mata_kuliah)";

        $this->db->query($query);
        $this->db->bind('id_dosen', $data['nama_dosen']);
        $this->db->bind('id_kelas', $data['kelas']);
        $this->db->bind('jadwal', $data['jadwal']);
        $this->db->bind('mata_kuliah', $data['mata_kuliah']);

        $this->db->execute();

        return $this->db->rowCount();
    }

    // function edit data jadwal kelas
    public function editDataJadwal($data)
    {

        $query = "UPDATE jadwal_kelas SET
                id_dosen = :id_dosen, 
                id_kelas = :id_kelas,
                jadwal = :jadwal,
                mata_kuliah = :mata_kuliah
            WHERE id_jadwal = :id_jadwal";

        $this->db->query($query);
        $this->db->bind('id_dosen', $data['nama_dosen']);
        $this->db->bind('id_kelas', $data['kelas']);
        $this->db->bind('jadwal', $data['jadwal']);
        $this->db->bind('mata_kuliah', $data['mata_kuliah']);
        $this->db->bind('id_jadwal', $data['id_jadwal']);

        $this->db->execute();

        return $this->db->rowCount();
    }

    // function hapus data jadwal kelas
    public function hapusDataJadwal($id)
    {
        $query = "DELETE FROM jadwal_kelas WHERE id_jadwal =:id_jadwal";
        $this->db->query($query);
        $this->db->bind('id_jadwal', $id);

        $this->db->execute();

        return $this->db->rowCount();
    }
}
?>