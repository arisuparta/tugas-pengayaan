<?php 

class Kelas_model
{
    private $table = 'kelas';
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    // function menampilkan semua data Kelas
    public function getAllKelas()
    {
        $this->db->query('SELECT * FROM ' . $this->table);
        return $this->db->resultSet();
    }

    // function menampilkan data kelas berdasarkan id
    public function getKelasByID($id)
    {
        $this->db->query('SELECT * FROM ' . $this->table . ' WHERE id_kelas=:id_kelas ');
        $this->db->bind('id_kelas', $id);
        return $this->db->single();
    }

    // function menambah data kelas
    public function tambahDataKelas($data)
    {

        $query = "INSERT INTO kelas
                    VALUES
                    (null,:nama_kelas, :prodi, :fakultas)";

        $this->db->query($query);
        $this->db->bind('nama_kelas', $data['nama']);
        $this->db->bind('prodi', $data['prodi']);
        $this->db->bind('fakultas', $data['fakultas']);

        $this->db->execute();

        return $this->db->rowCount();
    }

     // function edit data kelas
     public function editDataKelas($data)
     {
 
         $query = "UPDATE kelas SET
                    nama_kelas = :nama_kelas,
                    prodi = :prodi,
                    fakultas = :fakultas
                WHERE id_kelas = :id_kelas";
 
         $this->db->query($query);
         $this->db->bind('nama_kelas', $data['nama']);
         $this->db->bind('prodi', $data['prodi']);
         $this->db->bind('fakultas', $data['fakultas']);
         $this->db->bind('id_kelas', $data['id_kelas']);
 
         $this->db->execute();
 
         return $this->db->rowCount();
     }

     // function hapus data kelas
    public function hapusDataKelas($id)
    {
        $query = "DELETE FROM kelas WHERE id_kelas =:id_kelas";
        $this->db->query($query);
        $this->db->bind('id_kelas', $id);

        $this->db->execute();

        return $this->db->rowCount();
    }
}
?>