<div class="container">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
           <h1 class="h3 mb-0 text-gray-800">Tambah Data Kelas</h1>
        </div>
            <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Masukkan Data Kelas dengan Benar</h6>
                    </div>
                <div class="card-body">
                    <form action="<?= BASEURL; ?>/kelas/tambah" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="id_kelas" id="id_kelas">
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama">
                    </div>
                    <div class="form-group">
                        <label for="prodi">Program Studi</label>
                        <input type="prodi" class="form-control" id="prodi" name="prodi">
                    </div>
                    <div class="form-group">
                        <label for="fakultas">Fakultas</label>
                        <input type="fakultas" class="form-control" id="fakultas" name="fakultas">
                    </div>
                </div>
                <div class="footer mb-4 px-3">
                    <button type="submit" class="btn btn-primary">Tambah Data</button>
                    </form>
                </div>
            </div>
</div>
