<div class="container">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
           <h1 class="h3 mb-0 text-gray-800">Tambah Data Jadwal Kelas</h1>
        </div>
            <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Masukkan Data Jadwal Kelas dengan Benar</h6>
                    </div>
                <div class="card-body">
                    <form action="<?= BASEURL; ?>/jadwal/tambah" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="id_jadwal" id="id_kelas">
                    <div class="form-group">
                        <label for="nama">Dosen Pengampu</label>
                        <select class="form-control" id="nama_dosen" name="nama_dosen">
                            <option value="">Pilih Dosen</option>
                            <?php foreach ($data['dosen'] as $dsn) : ?>
                                <option value="<?= $dsn['id_dosen']; ?>"><?= $dsn['nama_dosen']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="kelas">Kelas</label>
                        <select class="form-control" id="kelas" name="kelas">
                            <option value="">Pilih Kelas</option>
                            <?php foreach ($data['kelas'] as $kls) : ?>
                                <option value="<?= $kls['id_kelas']; ?>"><?= $kls['nama_kelas']; ?></option>
                                <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="jadwal">Jadwal</label>
                        <input type="datetime-local" class="form-control" id="jadwal" name="jadwal">
                    </div>
                    <div class="form-group">
                        <label for="mata_kuliah">Mata Kuliah</label>
                        <input type="text" class="form-control" id="mata_kuliah" name="mata_kuliah">
                    </div>
                </div>
                <div class="footer mb-4 px-3">
                    <button type="submit" class="btn btn-primary">Tambah Data</button>
                    </form>
                </div>
            </div>
</div>
