<div class="container mt-3">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
       <h1 class="h3 mb-0 text-gray-800">Data Jadwal Kelas</h1>
    </div>

    <div class="row mb-3">
        <div class="col-lg-6">
            <a href="<?= BASEURL; ?>/jadwal/inputJadwal">
            <button type="button" class="btn btn-primary">
                Tambah Data Jadwal Kelas
            </button>
            </a>
        </div>
    </div>


    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Data Jadwal Kelas</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table border-secondary" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Nama Dosen</th>
                                            <th>Kelas</th>
                                            <th>Jadwal</th>
                                            <th>Mata Kuliah</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($data['jadwal'] as $jdl) : ?>
                                        <tr>
                                            <td class="align-middle"><?= $jdl['nama_dosen']; ?></td>
                                            <td class="align-middle"><?= $jdl['nama_kelas']; ?></td>
                                            <td class="align-middle"><?= $jdl['jadwal']; ?></td>
                                            <td class="align-middle"><?= $jdl['mata_kuliah']; ?></td>
                                            <td class="align-middle">
                                                <a class="btn btn-success px-3 py-2 badge badge-success float-left ml-1" href="<?= BASEURL; ?>/jadwal/getEdit/<?= $jdl['id_jadwal']; ?>" role="button"><i class="fas fa-edit mr-2"></i> Edit</a>
                                                <a class="btn btn-danger px-3 py-2 badge badge-danger float-left ml-1" href="<?= BASEURL; ?>/jadwal/hapus/<?= $jdl['id_jadwal']; ?>" role="button" onclick="return confirm('Yakin?');"><i class="fas fa-trash-alt mr-2"></i>Hapus</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
</div>
