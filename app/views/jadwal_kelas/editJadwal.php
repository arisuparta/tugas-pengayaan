<?php $jadwal = $data['jadwal']  ?>
<?php $newDate = date('Y-m-d\TH:i', strtotime($jadwal['jadwal'])); ?>
<div class="container">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
           <h1 class="h3 mb-0 text-gray-800">Edit Data Jadwal Kelas</h1>
        </div>
            <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Masukkan Data Jadwal Kelas dengan Benar</h6>
                    </div>
                <div class="card-body">
                    <form action="<?= BASEURL; ?>/jadwal/edit" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="id_jadwal" id="id_kelas" value="<?= $jadwal['id_jadwal']; ?>">
                    <div class="form-group">
                        <label for="nama">Dosen Pengampu</label>
                        <select class="form-control" id="nama_dosen" name="nama_dosen">
                            <option value="">Pilih Dosen</option>
                            <?php foreach ($data['dosen'] as $dsn) : ?>
                                <?php if($dsn['id_dosen'] == $jadwal['id_dosen']) : ?>
                                    <option value="<?= $dsn['id_dosen']; ?>" selected><?= $dsn['nama_dosen']; ?></option>
                                <?php else: ?>
                                    <option value="<?= $dsn['id_dosen']; ?>"><?= $dsn['nama_dosen']; ?></option>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="kelas">Kelas</label>
                        <select class="form-control" id="kelas" name="kelas">
                            <option value="">Pilih Kelas</option>
                            <?php foreach ($data['kelas'] as $kls) : ?>
                                <?php if($kls['id_kelas'] == $jadwal['id_kelas']) : ?>
                                    <option value="<?= $kls['id_kelas']; ?>" selected><?= $kls['nama_kelas']; ?></option>
                                <?php else: ?>
                                    <option value="<?= $kls['id_kelas']; ?>"><?= $kls['nama_kelas']; ?></option>
                                <?php endif; ?>
                                
                                <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="jadwal">Jadwal</label>
                        <input type="datetime-local" class="form-control" id="jadwal" name="jadwal" value="<?= $newDate; ?>">
                    </div>
                    <div class="form-group">
                        <label for="mata_kuliah">Mata Kuliah</label>
                        <input type="text" class="form-control" id="mata_kuliah" name="mata_kuliah" value="<?= $jadwal['mata_kuliah']; ?>">
                    </div>
                </div>
                <div class="footer mb-4 px-3">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
</div>
