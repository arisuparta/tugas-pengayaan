<div class="container">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
           <h1 class="h3 mb-0 text-gray-800">Tambah Data Dosen</h1>
        </div>
            <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Masukkan Data Dosen dengan Benar</h6>
                    </div>
                <div class="card-body">
                    <form action="<?= BASEURL; ?>/dosen/tambah" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="id" id="id">
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama">
                    </div>
                    <div class="form-group">
                        <label for="nip">NIP</label>
                        <input type="number" class="form-control" id="nip" name="nip">
                    </div>
                    <div class="form-group">
                        <label for="prodi">Prodi</label>
                        <input type="prodi" class="form-control" id="prodi" name="prodi">
                    </div>
                    <div class="form-group">
                        <label for="fakultas">Fakultas</label>
                        <input type="fakultas" class="form-control" id="fakultas" name="fakultas">
                    </div>
                    <div class="form-group">
                            <label for="foto">Foto</label>
                            <div class="custom-file">
                                <input type="file" class="" id="foto" name="foto">
                            </div>
                    </div>
                </div>
                <div class="footer mb-4 px-3">
                    <button type="submit" class="btn btn-primary">Tambah Data</button>
                    </form>
                </div>
            </div>
</div>
