<div class="container mt-3">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
       <h1 class="h3 mb-0 text-gray-800">Data Dosen</h1>
    </div>

    <div class="row mb-3">
        <div class="col-lg-6">
            <a href="<?= BASEURL; ?>/dosen/inputDosen">
            <button type="button" class="btn btn-primary">
                Tambah Data Dosen
            </button>
            </a>
        </div>
    </div>


    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Data Dosen Fakultas Teknik dan Kejuruan</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table border-secondary" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Foto</th>
                                            <th>NIP</th>
                                            <th>Nama</th>
                                            <th>Program Studi</th>
                                            <th>Fakultas</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($data['dosen'] as $dsn) : ?>
                                        <tr>
                                            <td><img src="<?= BASEURL; ?>/img/foto_dosen/<?= $dsn['foto_dosen']; ?>" alt="<?= $dsn['foto_dosen']; ?>" width="150px" class="img-thumbnail rounded-1"></td>
                                            <td class="align-middle"><?= $dsn['nip_dosen']; ?></td>
                                            <td class="align-middle"><?= $dsn['nama_dosen']; ?></td>
                                            <td class="align-middle"><?= $dsn['prodi']; ?></td>
                                            <td class="align-middle"><?= $dsn['fakultas']; ?></td>
                                            <td class="align-middle">
                                                <a class="btn btn-success px-3 py-2 badge badge-success float-left ml-1" href="<?= BASEURL; ?>/dosen/getEdit/<?= $dsn['id_dosen']; ?>" role="button"><i class="fas fa-edit mr-2"></i> Edit</a>
                                                <a class="btn btn-danger px-3 py-2 badge badge-danger float-left ml-1" href="<?= BASEURL; ?>/dosen/hapus/<?= $dsn['id_dosen']; ?>" role="button" onclick="return confirm('Yakin?');"><i class="fas fa-trash-alt mr-2"></i>Hapus</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
</div>
