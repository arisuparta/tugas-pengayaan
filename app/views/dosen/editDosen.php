<div class="container">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
           <h1 class="h3 mb-0 text-gray-800">Tambah Data Dosen</h1>
        </div>
            <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Masukkan Data Dosen dengan Benar</h6>
                    </div>
                <div class="card-body">
                    <?php $dosen = $data['dosen']  ?>
                    <form action="<?= BASEURL; ?>/dosen/edit" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="id_dosen" id="id_dosen" value="<?= $dosen['id_dosen']; ?>">
                    <input type="hidden" name="gambar" id="gambar" value="<?= $dosen['foto_dosen']; ?>">
                    <div class="form-group">
                        <img src="<?= BASEURL; ?>/img/foto_dosen/<?= $dosen['foto_dosen']; ?>" alt="<?= $dosen['foto_dosen']; ?>" width="150px" class="img-thumbnail rounded-1">
                        <div class="custom-file">
                        <input type="file" class="" id="foto" name="foto">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama" value="<?= $dosen['nama_dosen']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="nip">NIP</label>
                        <input type="number" class="form-control" id="nip" name="nip" value="<?= $dosen['nip_dosen']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="prodi">Prodi</label>
                        <input type="prodi" class="form-control" id="prodi" name="prodi" value="<?= $dosen['prodi']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="fakultas">Fakultas</label>
                        <input type="fakultas" class="form-control" id="fakultas" name="fakultas" value="<?= $dosen['fakultas']; ?>">
                    </div>
                </div>
                <div class="footer mb-4 px-3">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
</div>
