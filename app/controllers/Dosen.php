<?php 

class Dosen extends Controller
{
    // index dosen
    public function index()
    {
        $data['judul'] = 'Dosen';
        $data['active'] = 'dosen';
        $data['dosen'] = $this->model('Dosen_model')->getAllDosen();
        $this->view('templates/header', $data);
        $this->view('dosen/index', $data);
        $this->view('templates/footer', $data);
    }

    // form input dosen
    public function inputDosen()
    {
        $data['judul'] = 'Tambah Dosen';
        $data['active'] = 'dosen';
        $this->view('templates/header', $data);
        $this->view('dosen/inputDosen', $data);
        $this->view('templates/footer', $data);
    }

    // funtion tambah Dosen
    public function tambah()
    {
        if ($this->model('Dosen_model')->tambahDataDosen($_POST) > 0) {
            header('Location: ' . BASEURL . '/dosen');
            exit;
        } else {
            header('Location: ' . BASEURL . '/dosen');
            exit;
        }
    }

    // form edit
    public function getEdit($id)
    {
        $data['judul'] = 'Edit Data Dosen';
        $data['active'] = 'dosen';
        $data['dosen'] = $this->model('Dosen_model')->getDosenById($id);
        $this->view('templates/header', $data);
        $this->view('dosen/editDosen', $data);
        $this->view('templates/footer', $data);
    }

    // function edit dosen
    public function edit()
    {
        if ($this->model('Dosen_model')->editDataDosen($_POST) > 0) {
            header('Location: ' . BASEURL . '/dosen');
            exit;
        } else {
            header('Location: ' . BASEURL . '/dosen');
            exit;
        }
    }

    // hapus data dosen
    public function hapus($id)
    {
        if ($this->model('Dosen_model')->hapusDataDosen($id) > 0) {
            header('Location: ' . BASEURL . '/dosen');
            exit;
        } else {
            header('Location: ' . BASEURL . '/dosen');
            exit;
        }
    }

}
?>