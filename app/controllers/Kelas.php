<?php 

class Kelas extends Controller {

    // index kelas
    public function index()
    {
        $data['judul'] = 'Kelas';
        $data['kelas'] = $this->model('Kelas_model')->getAllKelas();
        $this->view('templates/header', $data);
        $this->view('kelas/index', $data);
        $this->view('templates/footer', $data);
    }

    // form input kelas
    public function inputKelas()
    {
        $data['judul'] = 'Tambah Kelas';
        $this->view('templates/header', $data);
        $this->view('kelas/inputKelas', $data);
        $this->view('templates/footer', $data);
    }

    // funtion tambah Kelas
    public function tambah()
    {
        if ($this->model('Kelas_model')->tambahDataKelas($_POST) > 0) {
            header('Location: ' . BASEURL . '/kelas');
            exit;
        } else {
            header('Location: ' . BASEURL . '/kelas');
            exit;
        }
    }

    // form edit
    public function getEdit($id)
    {
        $data['judul'] = 'Edit Data Kelas';
        $data['kelas'] = $this->model('Kelas_model')->getKelasById($id);
        $this->view('templates/header', $data);
        $this->view('kelas/editKelas', $data);
        $this->view('templates/footer', $data);
    }

    // function edit kelas
    public function edit()
    {
        if ($this->model('Kelas_model')->editDataKelas($_POST) > 0) {
            header('Location: ' . BASEURL . '/kelas');
            exit;
        } else {
            header('Location: ' . BASEURL . '/kelas');
            exit;
        }
    }

    // hapus data kelas
    public function hapus($id)
    {
        if ($this->model('Kelas_model')->hapusDataKelas($id) > 0) {
            header('Location: ' . BASEURL . '/kelas');
            exit;
        } else {
            header('Location: ' . BASEURL . '/kelas');
            exit;
        }
    }
}
?>