<?php 

class Jadwal extends Controller{

    // index Jadwal
    public function index()
    {
        $data['judul'] = 'Jadwal Kelas';
        $data['jadwal'] = $this->model('Jadwal_model')->getAllJadwal();
        $this->view('templates/header', $data);
        $this->view('jadwal_kelas/index', $data);
        $this->view('templates/footer', $data);
    }

     // form input jadwal kelas
     public function inputJadwal()
     {
         $data['judul'] = 'Tambah Kelas';
         $data['dosen'] = $this->model('Dosen_model')->getAllDosen();
         $data['kelas'] = $this->model('Kelas_model')->getAllKelas();
         $this->view('templates/header', $data);
         $this->view('jadwal_kelas/inputJadwal', $data);
         $this->view('templates/footer', $data);
     }

      // funtion tambah jadwal
    public function tambah()
    {
        if ($this->model('Jadwal_model')->tambahDataJadwal($_POST) > 0) {
            header('Location: ' . BASEURL . '/jadwal');
            exit;
        } else {
            header('Location: ' . BASEURL . '/jadwal');
            exit;
        }
    }

    // form edit
    public function getEdit($id)
    {
        $data['judul'] = 'Edit Data Jadwal Kelas';
        $data['jadwal'] = $this->model('Jadwal_model')->getJadwalById($id);
        $data['dosen'] = $this->model('Dosen_model')->getAllDosen();
        $data['kelas'] = $this->model('Kelas_model')->getAllKelas();
        $this->view('templates/header', $data);
        $this->view('jadwal_kelas/editJadwal', $data);
        $this->view('templates/footer', $data);
    }

    // function edit jadwal kelas
    public function edit()
    {
        if ($this->model('Jadwal_model')->editDataJadwal($_POST) > 0) {
            header('Location: ' . BASEURL . '/jadwal');
            exit;
        } else {
            header('Location: ' . BASEURL . '/jadwal');
            exit;
        }
    }
 
    // hapus data jadwal
    public function hapus($id)
    {
        if ($this->model('Jadwal_model')->hapusDataJadwal($id) > 0) {
            header('Location: ' . BASEURL . '/jadwal');
            exit;
        } else {
            header('Location: ' . BASEURL . '/jadwal');
            exit;
        }
    }
}
?>